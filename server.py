#!/usr/bin/env python

import socket
import signal
import os
import subprocess
import threading
import sys
import time
import select


HOST = ''
PORT = 1337
BACKLOG = 10

CHAT_LOG = "log.txt"
HISTORY_LEN = 512
CONNECTION_LIST = []

"""MSG64-server: a base64 encoded chat client."""

__author__		= "Benjamin Donnelly"
__email__		= "accipitercaelum@gmail.com"
__status__		= "Development"

"""
***Protocol Description***
1) Server Listens for connection
#2) Client initiates connection with "hello"
#3) Server issues authentication challenge (server password) + salt
#4) Client responds with server password
#5) Server issues authentication challenge (user/(pass) + salt)
#6) Server issues session id, logs user into chat

**Client requests**
VIEW -- returns chat log
SEND <message> <session id> -- append <message> to chat log
"""

def clear():
	if os.name == "posix":
		subprocess.call("clear",shell=True)
	elif os.name == "nt":
		subprocess.call("cls",shell=True)
	else:
		return False
	return True

def initiate_log():
	fi = open(CHAT_LOG,"a")
	fi.write(" ")

def clean_log():
	num = sum(1 for line in open(CHAT_LOG))
	if num > 512:
		bak = open("bak.log","w")
		fi = open(CHAT_LOG,"r")
		data = fi.read()
		bak.write(data)
		bak.close()
		fi.close()
		
		del_fi = open(CHAT_LOG,"w")
		del_fi.write("")

def parse_response(data, address):
	if "VIEW" in data[0:4]:
		return open(CHAT_LOG,"r").read()
	elif "SEND" in data[0:4]:
		open(CHAT_LOG,"a").write("\n" + str(address) + " :: " + " ".join(data.split(" ")[1:]))
		return "sent"
	return "Protocol Mismatch"

def server():
	s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
	s.bind((HOST, PORT))
	s.listen(BACKLOG)
	s.setblocking(0)
	
	CONNECTION_LIST.append(s)
	
	while True:
		read_sockets,write_sockets,error_sockets = select.select(CONNECTION_LIST, [], [])
		
		for sock in read_sockets:
		
			if sock == s:
				sockfd, addr = s.accept()
				CONNECTION_LIST.append(sockfd)
				print "Client (%s, %s) connected" % addr
			
			else:
				try:
					data = sock.recv(256)
					if data:
						sock.send(parse_response(data, addr))
				except:
					print "Client (%s, %s) is offline" % addr
					sock.close()
					CONNECTION_LIST.remove(sock)
					continue
	
	s.close()
		
def signal_handler(signum, frame):
	sys.exit(0)

#Begin main server loop
if __name__ == "__main__":
	signal.signal(signal.SIGINT, signal_handler)
	clear()
	initiate_log()
	
	threads = []
	t = threading.Thread(target=server)
	threads.append(t)
	t.daemon = True
	t.start()
	print "Server initiated "
	while True:
		time.sleep(1)
		clean_log()